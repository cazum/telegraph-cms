<?php
/*
Telegraph CMS by Cazum (zombiearmy.net)
This file is released in the public domain
NO WARRANTY PROVIDED
*/
/*
	TODO:
	*start working on a way to upload/handle images (store them in a public folder with a collision-free naming convention (idobf.php)
	*image organizer tab
	*work on saving drafts, and displaying drafts in a seperate context. (seperate posts table "draft_posts")
	*implement tags!
	*jquery automatic draft saving, and no-refresh markdown viewing while writing posts
	*tag searching in displayOldPosts
	*implement markdown
	*reorganize functions to work with a seperate website (telegraph.php)
	*each time a post is requested by anything other than the admin panel, save a number to a stats.sqlite for stat tracking (disableable)
	*customizable home screen w/ stats & graphs and stuff
	*user settings
	*switch config files to a json scheme
	*
*/
include('func/bcrypt.php');
include('func/model.php');
include('func/view.php');
include('func/general.php');
include('func/idobf.php');
include('func/paginate.php');
include('func/request.php');
include('func/user.php');
include('config/global.conf.php');

if(file_exists (LOGIN_CONFIG_LOCATION)) {
	$cred = readJsonFile("config/login.conf.php");
	$GLOBALS['db_name'] =  $cred['dbname'];
	unset($cred);
}

include('template/header.php');

session_start();
$GLOBALS['errorlog'] = array();
$GLOBALS['general_message'] = array();

$user = new user();

if(!$user->isLoggedIn()) {
	if(!file_exists(LOGIN_CONFIG_LOCATION)) {
		if(Request::getGet('a') == "register") {
			if(checkRegister()) {
				//check if folder db exists, if not, prompt user to create it
				if(!file_exists ( "db" )){
					displayFatalError("folder 'db' not found. Please create it with sufficient read/write permissions."); 
					die();
				}
				$GLOBALS['db_name'] = Request::getPost('db_name');
				$db = new Database();
				$succesfullyConnected = $db->success;
				unset($db);
				
				if($succesfullyConnected) {//if the DB is able to connect
					if($user->saveCredentials(Request::getPost('username'),Request::getPost('passone'),Request::getPost('db_name'))!=false) {//if the config file saves
					
						$GLOBALS['db_name'] = Request::getPost('db_name');
						$db = new Database();
						$db->createTables();
						unset($db);
						displayLoginScreen();
					}
					else {
						catchError("Failed to save login config file. Check file permissions.",0);
						displayRegisterScreen();
					}
				}
				else {
					displayRegisterScreen();
				}
			}
			else {//if register failed
				displayRegisterScreen();
			}
		}
		else {//if opens page without register request, but still not initialized (aka the very first time you visit the page)
			$checkmodules = checkmodules();
			
			if ($checkmodules === true) {//checks to make sure you have sql and everything installed
				displayRegisterScreen();//show register page
			}
			else {
				displayFatalError($checkmodules); //show a fatal error
			}
		}
	}
	else {//if already registered (aka: config file already created)
		if(Request::getGet('a') == "login") {//if submitted login request
			if($user->checkLogin()) {
				//success, show admin panel
				$_SESSION['timeout'] = time();//record the time we logged in at
			}
			else {//login failed
				displayLoginScreen();
			}
		}
		else {//config is already initialized, but we havent sent a login request yet
			displayLoginScreen(); //display the login prompt
		}
	}
}
else {
	if($_SESSION['timeout'] + SESSION_TIMEOUT_TIME < time()) {
		//TODO: check if the user is trying to save a post, and if they are, let it go through then logout.
		$user->logout();
	}
	else {
		switch (Request::getGet('a')) {
		case "logout":
			$user->logout();
			break;
		case "editor":
			displayEditorScreen();
			break;
		case "oldposts":
			displayOldPostsScreen();
			break;
		case "settings":
			displayHomeScreen();
			break;
		case "help":
			displayHelpScreen();
			break;
		case "savepost":
			if(Posts::savePost(Request::getPost('post_title'), "../images/thumb.jpg" , Request::getPost('post_content'), Request::getPost('post_tags'))) {
				displayOldPostsScreen("last");
				break;
			}
			displayEditorScreen();
			break;
		case "editpost":
			$x = new Post(Request::getGet('id'));
			if($x->update(Request::getPost('post_title'), "../images/thumb.jpg" , Request::getPost('post_content'), Request::getPost('post_tags'))) {
				setMessage("Post succesfully updated!");
			}
			else {
				setMessage("Post edit failed!");
			}
			unset($x);
			displayOldPostsScreen();
			break;
		case "deletepost":
		
			$x = new Post(Request::getGet('id'));
			
			if($x->delete()) {
				setMessage("Succesfully deleted!");
				displayOldPostsScreen();
			}
			else {
				setMessage("Failed to delete. Unknown post ID: ". Request::getGet('id'), "danger");
				displayEditorScreen();
			}
			unset($x);
			break;
		default:
			displayHomeScreen();
		}
	}
}
include('template/footer.php');
?>