<?php

function checkRegister() {//Check if register credentials are valid
	if(Request::getPost("username","passone","passtwo","db_name")!=false) {
		$username = Request::getPost("username");
		$passone = Request::getPost("passone");
		$passtwo = Request::getPost("passtwo");
		$db_name = Request::getPost("db_name");
	
		if(strlen($username)<=3) {//username is 4 chars or more
			catchError("Username must be 4 or more characters in length", 1);
			return false;
		}
		elseif(preg_match("/[^a-zA-Z0-9-_]+/i",$username)!=0) {//if username contains illegal characters
			catchError("Username may only contain letters, numbers, dashes and underscores.", 1);
			return false;
		}
		elseif(strlen($passone)<=5) {//if password is 6 chars or more
			catchError("Password must be 6 or more characters in length", 2);
			return false;
		}
		elseif(md5($passone)!=md5($passtwo)) {//if password was not typed correctly twice
			catchError("Passwords must match", 3);
			return false;
		}
		elseif($db_name=="") {//if field is empty
			catchError("Must give a database name", 5);
			return false;
		}
		else {//no problems found
			return true;
		}
	}
	catchError("POST error occurred. Please try again",0);
	return false;
}

function checkModules() {//Checks if necessary php modules are installed
	$output_pre = "Failed to load the following ";
	$output_post = " necessary PHP module(s): ";
	$fails = 0;
	
	if(!extension_loaded('PDO')) {
		$output_post .= "[PDO] ";
		$fails += 1;
	}
	if(!extension_loaded('date')) {
		$output_post .= "[date] ";
		$fails += 1;
	}
	if(!extension_loaded('session')) {
		$output_post .= "[session] ";
		$fails += 1;
	}
	if(!extension_loaded('pdo_sqlite')) {
		$output_post .= "[pdo_sqlite] ";
		$fails += 1;
	}
	
	if($fails==0) {
		return true;
	}
	else {
		return $output_pre. $fails. $output_post;
	}
}

function getURIArg($arg) {
	if(isset($_GET[$arg])) {
		return $_GET[$arg];
	}
	return null;
}

function saveConfig($username, $pwhash, $db_name) {
	$arr = array();
	$arr['username'] = $username;
	$arr['pwhash'] = $pwhash;
	$arr['dbname'] = $db_name;
	return file_puts_contents(LOGIN_CONFIG_LOCATION,json_encode($arr));
	/*
	$text="";
	$text = "<?php\n";
	$text .= "\$username = " .var_export($username,true) .";\n";
	$text .= "\$pwhash = " .var_export(bcrypt_hash($pwhash),true) .";\n";
	$text .= "\$db_name = " .var_export($db_name,true) .";\n";
	$text .= "?>";
	return file_put_contents(LOGIN_CONFIG_LOCATION,$text);
	*/
}

//function modifyconfig($variable,$value)

function setMessage($message, $statement = "info") {
	$GLOBALS['general_message'] = array($message, $statement);
}
function catchError($message, $position) {
	$GLOBALS['errorlog'] = array($message,$position);
}

function readJsonFile($filepath) {
	$data = json_decode(substr(file_get_contents($filepath),8),true);
	return $data;
}

function saveJsonFile($filepath,$dataarray) {
	return file_put_contents($filepath,"<?php //".json_encode($dataarray));
}
?>