<?php 
class user {
	
	function isLoggedIn() {
		if(isset($_SESSION['userid'])) {
			if($_SESSION['userid'] == "admin") {
				return true;
			}
		}
		return false;
	}
	
	function checkLogin() {
		$cred = readJsonFile("config/login.conf.php");
		$realUsername =  $cred['username'];
		$realPwhash =  $cred['pwhash'];
		unset($cred);
		
		if(Request::getPost("username","password")!=false) {
		$username = Request::getPost("username");
		$password = Request::getPost("password");

			sleep(1.25);
			if($username==$realUsername && bcrypt_check($password, $realPwhash)) {
				$_SESSION['userid'] = "admin";
				displayHomeScreen();
				return true;
			}
			else {
				catchError("Username or Password incorrect",0);
				return false;
			}
		}
		return false;
	}
	
	function saveCredentials($username, $pwhash, $db_name) {
		$arr = array();
		$arr['username'] = $username;
		$arr['pwhash'] = bcrypt_hash($pwhash);
		$arr['dbname'] = $db_name;
		
		return saveJsonFile(LOGIN_CONFIG_LOCATION, $arr);
	}
	
	function logout() {
		session_destroy();
		displayLogoutScreen();
		return true;
	}

}