<?php
/*
obfuscates a numeric value to a human unreadable string, but something a computer can read eaily.
minimum size: 5 characters;
pad with zeros in front of usable number
*/

function id_obfuscate($id) {

	$obfuscator_values = array(array("0", "a" , "k", "u", "v", "w", "x", "y", "z"), array("1", "b" , "l"), array("2", "c" , "m"), array("3", "d" , "n"), array("4", "e" , "o"), array("5", "f" , "p"), array("6", "g" , "q"), array("7", "h" , "r"), array("8", "i" , "s"), array("9", "j" , "t"));	
	$id = strval($id);
	$output = "";
	$len = strlen($id);
	
	for($i=0;$i<5-$len;$i++) {
		$id = "0".$id;
	}
	
	$chunk_array = str_split($id);
	
	foreach($chunk_array as $chunk) {
		$arr_maxsize = count($obfuscator_values[intval($chunk)])-1;
		$output .= $obfuscator_values[intval($chunk)][rand(0,$arr_maxsize)];
	}
	
	return $output;
}
 
function id_deobfuscate($str) {

	$obfuscator_values = array(array("0", "a" , "k", "u", "v", "w", "x", "y", "z"), array("1", "b" , "l"), array("2", "c" , "m"), array("3", "d" , "n"), array("4", "e" , "o"), array("5", "f" , "p"), array("6", "g" , "q"), array("7", "h" , "r"), array("8", "i" , "s"), array("9", "j" , "t"));	
	$chunk_array = str_split((string)$str);
	$output = "";
	
	foreach($chunk_array as $chunk) {
	
		$index = 0;
		
		foreach($obfuscator_values as $arr) {
			if(array_search($chunk,$arr)!==false) {	
				$output .= $index;
				break;
			}
			
			$index++;
		}
	}
	return (int)$output;
}
?>
