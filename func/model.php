<?php

class Database extends PDO {
	public $success;
	
	public function __construct() {
		$con = 'sqlite:db/' . $GLOBALS['db_name'] . '.sqlite';
		parent::__construct($con);
		
		if($con==false) {
			$this->success=false;
		}
		else {
			$this->success=true;
		}
	}
	
	public function createTables() {
		self::exec("CREATE TABLE IF NOT EXISTS posts ( id INTEGER PRIMARY KEY, obf_id TEXT, title TEXT, thumbnail TEXT, rawcontent TEXT, htmlcontent TEXT, first_time DATETIME DEFAULT now, edited_time DATETIME DEFAULT now);");
		self::exec("CREATE TABLE IF NOT EXISTS tags ( id INTEGER PRIMARY KEY, tag TEXT);");
		self::exec("CREATE TABLE IF NOT EXISTS tag_map ( post_id INTEGER, tag_id INTEGER);)");
		
		for($i=1;$i<=30;$i++) {//create some test data :)
			Posts::savePost("#".$i." Post", "..", "This is a sample post. Delete it and replace it with your own.", "pop, tags, yo");
		}
		unset($db);
		return true;
	}
}
class Posts {
	public static function getNumberOfPosts() {
		$db = new Database();
		$res = $db->query("SELECT COUNT(*) FROM posts");
		$ret = $res->fetchall(PDO::FETCH_NUM)[0][0];
		unset($db);
		return $ret;
	}
	public static function savePost($title, $thumbnail, $rawcontent, $tags) {
		if($title==null) {
			catchError("Please enter a title.",0);
			return false;
		}
		
		if($rawcontent==null) {
			catchError("Please enter some content",1);
			return false;
		}	
		
		//handle markdown parsing here
		$db = new Database();
		if(!$db){
			displayFatalError("Failed to open database"); die("couldnt");unset($db);
		}
		$prep = $db->prepare("INSERT INTO posts (title, thumbnail, rawcontent, first_time) VALUES (:title, :thumbnail, :rawcontent, 'now');");
		$prep->bindParam(':title',$title);
		$prep->bindParam(':thumbnail',$thumbnail);
		$prep->bindParam(':rawcontent',$rawcontent);
		$prep->execute();
		//generate obf'd ID and insert it.
		$last_id = $db->lastInsertId();
		$obf_id = id_obfuscate($last_id);
		$db->query("UPDATE posts SET obf_id='".$obf_id."' WHERE id='".$last_id."';");
		//parse tags and save them in the tags table
		//link tags to post in the tag_map table
		unset($db);
		return true;
	}
	public static function editPost($index, $title, $thumbnail, $rawcontent, $tags) {
		//inject the values back into WHERE index = $index
	}
	public static function getPostsAsArray($offset, $amount, $order, $tagfilter = NULL) {
		$posts = array();
		$maxposts = self::getNumberOfPosts();
		
		if($order==="desc") {
			for($i=0;$i<$amount;$i++) {
				if($maxposts-$i-$offset>0) {
					$x = new Post();
					$x->getByIndex($i+$offset,"desc");
					$x->index = $maxposts-$i-$offset;
					array_push($posts, $x);
					unset($x);
				}
				else {
					break;
				}
			}
		}
		else {
			for($i=0;$i<$amount;$i++) {
				if($i+$offset+1<=$maxposts) {
					$x = new Post();
					$x->getByIndex($i+$offset);
					$x->index = $i+$offset+1;
					array_push($posts, $x);
					unset($x);
				}
				else {
					break;
				}
			}
		}
		return $posts;
	}
}

class Post
{
	public $index;
	public $id; 
	public $obf_id;
	public $title;
	public $thumbnail;
	public $rawcontent;
	public $htmlcontent;
	public $date_created;
	public $date_edited;
	public $tags;//array
	
	public function __construct($id = null) {
		if($id!==null) {
			$this->getById($id);
		}
	}
	public function getByIndex($offset, $order = "asc") {
		$db = new Database();
		$res = $db->query("SELECT * FROM posts ORDER BY id " . $order . " LIMIT 1 OFFSET ". $offset);
		$ret = $res->fetchall(PDO::FETCH_ASSOC);
		if(empty($ret)) {
			unset($db);
			return false;
		}
		
		$this->id = $ret[0]['id'];
		$this->title = $ret[0]['title'];
		$this->obf_id = $ret[0]['obf_id'];
		$this->rawcontent = $ret[0]['rawcontent'];
		$this->date_created = $ret[0]['first_time'];
		unset($db);
		return true;
	}
	public function getById($id) {
		$id = id_deobfuscate($id);

		if($id>=1) {
			$db = new Database();
			$res = $db->query("SELECT * FROM posts WHERE id = " . $id);
			$ret = $res->fetchall(PDO::FETCH_ASSOC);

			if(empty($ret)) {
				unset($db);
				return false;
			}

			$this->id = $ret[0]['id'];
			$this->title = $ret[0]['title'];
			$this->obf_id = $ret[0]['obf_id'];
			$this->rawcontent = $ret[0]['rawcontent'];
			$this->date_created = $ret[0]['first_time'];
			//get tags from other table, and add them to an array $tags
			unset($db);
			return true;
		}
		return false;
	}
	public function update($title, $thumbnail, $rawcontent, $tags) {
		if($this->id==null) {
			//Show an error to the user
			return false;
		}
		if($title==null) {
			catchError("Please enter a title.",0);
			return false;
		}
		if($rawcontent==null) {
			catchError("Please enter some content",1);
			return false;
		}	
		
		//handle markdown parsing here
		$db = new Database();
		if(!$db){
			displayFatalError("Failed to open database"); die("couldnt");unset($db);
		}
		
		$prep = $db->prepare("UPDATE posts SET title=:title,thumbnail=:thumbnail,rawcontent=:rawcontent,first_time='now' WHERE id = :id");
		$prep->bindParam(':title',$title);
		$prep->bindParam(':thumbnail',$thumbnail);
		$prep->bindParam(':rawcontent',$rawcontent);
		$prep->bindParam(':id',$this->id);
		$prep->execute();
		//parse tags and save them in the tags table
		//link tags to post in the tag_map table
		unset($db);
		return true;
	}
	public function delete() {
		if($this->id==null) {
			//Show an error to the user
			return false;
		}
		
		$db = new Database();
		$prep = $db->prepare("DELETE FROM posts WHERE id = :id");
		$prep->execute( array( ":id" => $this->id ) );
		return true;
	}
}
?>