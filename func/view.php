<?php
//display general error
function displayError($position, $cssclass) {
	if(isset($GLOBALS['errorlog'][1])) {
		if($position == $GLOBALS['errorlog'][1]) {
		?>
			<span class="<?php echo $cssclass; ?>"><?php echo $GLOBALS['errorlog'][0]; ?></span>
		<?php
		}
	}
}

//display fatal error
function displayFatalError($message) {
?>
	<div class="uk-width-medium-1-3 uk-container-center">
		<h2>fatal error</h2>
		<?php echo $message ?>
	</div>
<?php
	
}

function displayMessage() {
	if(!$GLOBALS['general_message']) {
		return false;
	}
?>
	<div class="container">
		<div class="panel panel-<?php echo $GLOBALS['general_message'][1];?>">
			<div class="panel-body">
				<?php echo $GLOBALS['general_message'][0]; ?>
			</div>
		</div>
	</div>
<?php
}

function displayRegisterScreen() {
?>
	<div class="container">
		<div class="col-md-4 center-block promptbox">
			<h2>Register</h2>
			<?php displayError(0, "inputerror"); /* general error */ ?>
			<form class="uk-form" method="post" action="<?php $_SERVER['SCRIPT_NAME'] ?>?a=register" name="registerform" id="registerform">
				<fieldset>
					<h4>Account</h4>
					<div class="form-group">
						<?php displayError(1, "inputerror");/* username error */ ?>
						<input class="form-control" name="username" type="text" placeholder="Username" value="<?php if(Request::getPost('username')!=false){ echo preg_replace('/[^a-zA-Z0-9-_]+/i','',$_POST['username']);} ?>">
					</div>
					<div class="form-group">
						<?php displayError(2, "inputerror"); ?>
						<input class="form-control" name="passone" type="password" placeholder="Password">
					</div>
					<div class="form-group">
						<?php displayError(3, "inputerror"); ?>
						<input class="form-control" name="passtwo" type="password" placeholder="Repeat Password">
					</div>
					<div class="form-group">
						<h4>Database</h4>
					</div>
					<div class="form-group">
						<?php displayError(5, "inputerror"); ?>
						<input class="form-control" name="db_name" type="text" placeholder="Database Name">
					</div>
					<div class="form-group">
						<input class="form-control" name="db_timezone" type="text" placeholder="Timezone">
					</div>
					<div class="form-group">
						<input class="btn btn-default floatright" value="register" type="submit" />
					</div>
				</fieldset>
			</form>
		</div>
	</div>
<?php
}

function displayLoginScreen() {
?>
	<div class="container">
		<div class="col-md-4 center-block promptbox">
			<h2>login</h2>
			<?php displayError(0, "inputerror") ?>
			<form role="form" method="post" action="<?php $_SERVER['SCRIPT_NAME'] ?>?a=login" name="loginform" id="loginform">
				<div class="form-group">
					<input class="form-control"  placeholder="Username" name="username" type="text" value="<?php if(Request::getGet("username")!=false){ echo preg_replace("/[^a-zA-Z0-9-_]+/i",'',$_POST['username']);} ?>" >
					</div>
					<div class="form-group">
					<input class="form-control"  placeholder="Password" name="password" type="password">
					</div>
					<input class="btn btn-default btn-primary floatright" value="login" type="submit" >
				</fieldset>	
			</form>
		</div>
	</div>
<?php
}

function displayHeaderBar($active) {
?>
	<div class="navbar navbar-default navbar-static-top navbar-inverse" role="navigation">
	  <div class="container">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
		<div class="navbar-collapse collapse">
		  <ul class="nav navbar-nav">
					<li<?php if($active==1){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
					<li<?php if($active==2){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=editor"><span class="glyphicon glyphicon-plus"></span> New Post</a></li>
					<li<?php if($active==3){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=oldposts"><span class="glyphicon glyphicon-th-large"></span> Old Posts</a></li>
					<li<?php if($active==4){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=files"><span class="glyphicon glyphicon-paperclip"></span> Attachments</a></li>
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li<?php if($active==5){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=settings"><span class="glyphicon glyphicon-wrench"></span> Settings</a></li>
			<li<?php if($active==6){ ?> class="active" <?php } ?>><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=help"><span class="glyphicon glyphicon-info-sign"></span> Help</a></li>
			<li><a href="<?php $_SERVER['SCRIPT_NAME'] ?>?a=logout"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
		</ul>
		</div>
	  </div>
	</div>
<?php
displayMessage();
}
function displayHomeScreen() {
	displayHeaderBar(1);
?>
	<div class="container">
		<div class="panel panel-info"">
			<div class="panel-heading">
				<h3 class="panel-title">
					Home
				</h3>
			</div>
			<div class="panel-body">
				Home screen content
			</div>
		</div>
	</div>
<?php
}
function displayEditorScreen() {
	$edit=false;
	$id = Request::getGet("id");
	$post = new Post();

	if($id!=null && $post->getById($id)==true) {
		$edit=true;
		displayHeaderBar(3);
	}
	else {
		displayHeaderBar(2);
	}
?>
	<div class="container">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					Panel
					</h3>
				</div>
				<h5 class="form-subgroup">
					Meta
				</h5>
				<div class="panel-body">
					<div class="panel no-border">
						<a class="btn btn-default fullwidth" href="">Choose Thumbnail</a>
					</div>
					<div class="panel no-border">
						<a class="btn btn-default fullwidth" href="">Insert Image</a>
					</div>
					<div class="panel no-border">
						<a class="btn btn-default fullwidth" href="">Save Draft</a>
					</div>
				</div>
				<?php if($edit) { ?>
				<h5 class="form-subgroup">
					Edit post options
				</h5>
				<div class="panel-body">
					<div class="panel no-border">
						<a class="btn btn-default fullwidth" href="?a=deletepost<?php echo Request::formatGetList("id"); ?>">Delete Post</a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					<?php if($edit) { ?>
						Edit Post
					<?php } else {?>
						New Post
					<?php } ?>
					</h3>
				</div>
				<div class="panel-body">
				<?php
				if($edit) {
					$out = "editpost"."&id=".$id;
				}
				else {
					$out = "savepost";
				}
				?>
					<form class="uk-form" method="post" action="<?php $_SERVER['SCRIPT_NAME'] ?>?a=<?php echo $out; ?>">
						<?php displayError(0, "inputerror");?>
						<div class="panel no-border">
							<input name="post_title" type="text" class="form-control title_field" placeholder="Post Title" <?php if($edit==true){ ?>value="<?php echo $post->title;?>"<?php } ?>>
						</div>
						<fieldset>	
							<ul class="nav nav-tabs">
								<li class="active"><a href="#">markup</a></li>
								<li><a href="#">preview</a></li>
							</ul>
							<?php displayError(1, "inputerror");?>
							<textarea name="post_content" type="text" class="form-control no-border-radius rawcontent_field" placeholder="Post Content" ><?php if($edit==true){ echo $post->rawcontent; } ?></textarea>
							<div class="panel panel-default no-border-radius panel-under-content-field">
								<input name="post_tags" type="text" class="form-control" placeholder="Tags, Seperated, By, Commas" >
							</div>
							<div class="panel panel-default no-border-top-radius panel-under-tag-field">
								<div class="btn-group">
								<?php 
								if($edit) {
									$out = "Update Post";
								}
								else
								{
									$out = "Publish";
								}
								?>
									<input class="btn btn-primary" value="<?php echo $out; ?>" type="submit">
									<a class="btn btn-info" href="">Save As Draft</a>
									<a class="btn btn-danger" href="">Discard</a>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
<?php
	unset($post);
}
function displayOldPostsScreen($page = null) {

	$perpage = Request::getGet("perpage");
	
	if($page==null) {
		$page = Request::getGet("page");
	}
	
	$numberofposts = Posts::getNumberOfPosts();
	
	if($perpage!=2 && $perpage!=5 && $perpage!=10 && $perpage!=15) {
		$perpage=5;
	}
	
	if($page!="last") {
		if($page==NULL || $page<=0) {
			$page=1;
		}
	}
	
	$numberofpages = floor($numberofposts/$perpage);
	
	if($numberofposts%$perpage!=0) {
		$numberofpages+= 1;
	}
	
	if($page>$numberofpages) {
		$page=$numberofpages;
	}
	
	if($page=="last") {
		$page = $numberofpages;
	}
	
	$order = Request::getGet("order");
	
	if($order !== strtolower("desc")) {
		$order = "asc";
	}
	
	else {
		$order = "desc";
	}
	displayHeaderBar(3);
?>
	<div class="container">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					Filters
					</h3>
				</div>
				<h5 class="form-subgroup">
					Results per page
				</h5>
				<div class="panel-body">
					<ul class="pagination">
						<li <?php if($perpage==2){ ?> class="active" <?php } ?>><a href="?a=oldposts<?php echo Request::formatGetList("page","tag","order") ?>&perpage=2">2</a></li>
						<li <?php if($perpage==5){ ?> class="active" <?php } ?>><a href="?a=oldposts<?php echo Request::formatGetList("page","tag","order") ?>&perpage=5">5</a></li>
						<li <?php if($perpage==10){ ?> class="active" <?php } ?>><a href="?a=oldposts<?php echo Request::formatGetList("page","tag","order") ?>&perpage=10">10</a></li>
						<li <?php if($perpage==15){ ?> class="active" <?php } ?>><a href="?a=oldposts<?php echo Request::formatGetList("page","tag","order") ?>&perpage=15">15</a></li>
					</ul>
				</div>
				<h5 class="form-subgroup"><!-- autohide these elements, add an expand button, maybe add a thin border to the bottom/top of the header-->
					Filter by tag
				</h5>
				<div class="panel-body">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="tag">
						<span class="input-group-btn">
							<a href="" class="btn btn-primary" type="button">Add</a>
						</span>
					</div>
				</div>
				<h5 class="form-subgroup">
					List order
				</h5>
				<div class="panel-body">
					<div class="btn-group">
						<a class="btn<?php if($order==="asc"){ echo " btn-primary";}else{ echo " btn-default"; } ?>" href="?a=oldposts<?php echo Request::formatGetList("perpage","tag"); ?>">Ascending</a>
						<a class="btn<?php if($order==="desc"){ echo " btn-primary";}else{ echo " btn-default"; } ?>" href="?a=oldposts<?php echo Request::formatGetList("perpage","tag"); ?>&order=desc">Descending</a>
					</div>
				</div>
				<h5 class="form-subgroup">
					Other filters
				</h5>
				<div class="panel-body">
				filter options
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					Posts
					</h3>
				</div>
				<div class="panel-body">
					<?php foreach(Posts::getPostsAsArray(($page-1)*$perpage, $perpage, $order) as $post) { ?>
						<div class="panel panel-default">
							<div class="panel-body">
								<span class="badge">
								<?php echo $post->index;?>
								
								</span>
								<h4>
									<a href="?a=editor&id=<?php echo $post->obf_id; ?>"><?php echo $post->title; ?></a>
								</h4>
								<p>
								<?php echo nl2br($post->rawcontent); ?>
								
								</p>
							</div>
						</div>
					<?php unset($post); } ?>
					<ul class="pagination floatright">
						<?php foreach(getPaginateArray($page, $perpage, $numberofposts) as $pag) {
							$out = "<li";
							
							if($pag->active) {
								$out .= ' class="active"';
							}
							
							if($pag->disabled) {
								$out .= ' class="disabled';
								
								if($pag->direction===0) {
									$out .=' pagination_disabled';
								}
								
								$out .= '"';
							}
							$out .= '><a ';
							
							if($pag->direction==0 && $pag->disabled!=true) {
								$out .= 'href="?a=oldposts';
								$out .= Request::formatGetList("perpage","tag","order");
								$out .= '&page=';
								
								if($pag->direction==="left") { 
									$out .= $page-1; 
								}
								elseif($pag->direction==="right") {
									$out .= $page+1; 
								}
								else {
								$out .= $pag->pagenumber;
								}
								$out .= '"';
							}
							$out .= '>';
							$out .= $pag->pagenumber;
							$out .= '</a></li>';
							echo $out;
						} ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php
}
function displayHelpScreen()
{
	displayHeaderBar(6);
	?>
	<div class="container">
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					Navigation
					</h3>
				</div>
				<h5 class="form-subgroup">
					Posting
				</h5>
				<div class="panel-body">
					Ford<br>
					doop<br>
				</div>
				<h5 class="form-subgroup">
					Other
				</h5>
				<div class="panel-body">

				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Help
					</h3>
				</div>
				<div class="panel-body">
					<h1>telegraph</h1>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					<h3>about telegraph</h3>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					<h3>about telegraph</h3>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					<h3>about telegraph</h3>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</div>
			</div>
		</div>
	</div>
	<?php
}
function displayLogoutScreen()
{
	displayLoginScreen();
}
?>